﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Assigment2
{
    public partial class Form3 : Form
    {
        public Form3(string username)
        {
            InitializeComponent();
            account_txt.Text = username;
            Fillcombo2();
        }

        void fill_datagrid1()
        {
            string myConn2 = "datasource=localhost;username=root;password=";
            MySqlConnection conDataBase3 = new MySqlConnection(myConn2);
            MySqlCommand cmdDataBase2 = new MySqlCommand("select * from bug.bug where Assigned = '" + account_txt.Text + "' ;", conDataBase3);
            try
            {

                MySqlDataAdapter sda3 = new MySqlDataAdapter();
                sda3.SelectCommand = cmdDataBase2;
                DataTable dbdataset1 = new DataTable();
                sda3.Fill(dbdataset1);
                BindingSource bSource1 = new BindingSource();

                bSource1.DataSource = dbdataset1;
                dataGridView1.DataSource = bSource1;
                sda3.Update(dbdataset1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void Fillcombo2()
        {
            string constring = "datasource = localhost; username = root; password = ";
            string Query = "select * from bug.status where bug_status = 'Fixed' ";
            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand(Query, conDataBase);
            MySqlDataReader myReader;
            try
            {
                conDataBase.Open();
                myReader = cmdDataBase.ExecuteReader();

                while (myReader.Read())
                {

                    string status = myReader.GetString("bug_status");
                    status_combo.Items.Add(status);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void bug_view_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;
            bug_info_panel.Visible = true;
            string constring = "datasource=localhost;username=root;password=";
            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand("select * from bug.bug where Assigned = '"+account_txt.Text+"' ;", conDataBase);

            try
            {


                dataGridView1.Visible = true;
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView1.DataSource = bSource;
                sda.Update(dbdataset);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        } //view bug report accoridng to account

        private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];

                bug_id_txt.Text = row.Cells["Bug_ID"].Value.ToString();
                title_txt.Text = row.Cells["title"].Value.ToString();
                type_of_bug_txt.Text = row.Cells["Type_of_bug"].Value.ToString();
                software_txt.Text = row.Cells["software"].Value.ToString();
                software_version_txt.Text = row.Cells["software_version"].Value.ToString();
                description_txt.Text = row.Cells["description"].Value.ToString();
                step_to_reproduction_txt.Text = row.Cells["step_to_reproduction"].Value.ToString();
                severity_txt.Text = row.Cells["severity"].Value.ToString();
                priority_txt.Text = row.Cells["priority"].Value.ToString();
                symptom_txt.Text = row.Cells["symptom"].Value.ToString();
                assigned_txt.Text = row.Cells["Assigned"].Value.ToString();
            

            }
        } //display the data from dataGridView

        

        private void logout_button_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Hide();
            Form1 form1 = new Form1();
            form1.ShowDialog();
        } //user logout

        private void add_bug_Click(object sender, EventArgs e)
        {
            AddBugForm addbug = new AddBugForm();
            addbug.ShowDialog();
        }

        private void submit_Click(object sender, EventArgs e)
        {
            string constring = "datasource = localhost; username = root; password = ";
            string Query = "UPDATE bug.bug SET status='" + this.status_combo.Text + "' where Bug_ID = '" + this.bug_id_txt.Text + "'";
            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand(Query, conDataBase);
            MySqlDataReader myReader;
            try
            {
                conDataBase.Open();
                myReader = cmdDataBase.ExecuteReader();
                MessageBox.Show("The Bug has been closed");
                while (myReader.Read())
                {

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void refresh_button_Click(object sender, EventArgs e)
        {
            fill_datagrid1();
        }
    }
}
