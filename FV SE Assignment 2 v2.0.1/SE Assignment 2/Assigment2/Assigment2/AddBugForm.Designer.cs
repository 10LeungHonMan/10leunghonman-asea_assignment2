﻿namespace Assigment2
{
    partial class AddBugForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.title = new System.Windows.Forms.Label();
            this.Type = new System.Windows.Forms.Label();
            this.software = new System.Windows.Forms.Label();
            this.software_version = new System.Windows.Forms.Label();
            this.description = new System.Windows.Forms.Label();
            this.step_to_reproduction = new System.Windows.Forms.Label();
            this.severity = new System.Windows.Forms.Label();
            this.priority = new System.Windows.Forms.Label();
            this.symptom = new System.Windows.Forms.Label();
            this.title_txt = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.software_txt = new System.Windows.Forms.TextBox();
            this.software_version_txt = new System.Windows.Forms.TextBox();
            this.description_txt = new System.Windows.Forms.TextBox();
            this.step_to_reproduction_txt = new System.Windows.Forms.TextBox();
            this.severity_combo = new System.Windows.Forms.ComboBox();
            this.priority_combo = new System.Windows.Forms.ComboBox();
            this.symptom_txt = new System.Windows.Forms.TextBox();
            this.submit_button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.bugid_txt = new System.Windows.Forms.TextBox();
            this.cancel_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // title
            // 
            this.title.AutoSize = true;
            this.title.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title.Location = new System.Drawing.Point(95, 67);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(42, 23);
            this.title.TabIndex = 0;
            this.title.Text = "Title";
            // 
            // Type
            // 
            this.Type.AutoSize = true;
            this.Type.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Type.Location = new System.Drawing.Point(91, 109);
            this.Type.Name = "Type";
            this.Type.Size = new System.Drawing.Size(46, 23);
            this.Type.TabIndex = 1;
            this.Type.Text = "Type";
            // 
            // software
            // 
            this.software.AutoSize = true;
            this.software.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.software.Location = new System.Drawing.Point(80, 145);
            this.software.Name = "software";
            this.software.Size = new System.Drawing.Size(80, 23);
            this.software.TabIndex = 2;
            this.software.Text = "Software";
            // 
            // software_version
            // 
            this.software_version.AutoSize = true;
            this.software_version.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.software_version.Location = new System.Drawing.Point(40, 183);
            this.software_version.Name = "software_version";
            this.software_version.Size = new System.Drawing.Size(141, 23);
            this.software_version.TabIndex = 3;
            this.software_version.Text = "Software version";
            // 
            // description
            // 
            this.description.AutoSize = true;
            this.description.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.description.Location = new System.Drawing.Point(68, 223);
            this.description.Name = "description";
            this.description.Size = new System.Drawing.Size(99, 23);
            this.description.TabIndex = 4;
            this.description.Text = "Description";
            // 
            // step_to_reproduction
            // 
            this.step_to_reproduction.AutoSize = true;
            this.step_to_reproduction.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.step_to_reproduction.Location = new System.Drawing.Point(8, 387);
            this.step_to_reproduction.Name = "step_to_reproduction";
            this.step_to_reproduction.Size = new System.Drawing.Size(173, 23);
            this.step_to_reproduction.TabIndex = 5;
            this.step_to_reproduction.Text = "Step to Reproduction";
            // 
            // severity
            // 
            this.severity.AutoSize = true;
            this.severity.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.severity.Location = new System.Drawing.Point(80, 566);
            this.severity.Name = "severity";
            this.severity.Size = new System.Drawing.Size(72, 23);
            this.severity.TabIndex = 6;
            this.severity.Text = "Severity";
            // 
            // priority
            // 
            this.priority.AutoSize = true;
            this.priority.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.priority.Location = new System.Drawing.Point(85, 599);
            this.priority.Name = "priority";
            this.priority.Size = new System.Drawing.Size(67, 23);
            this.priority.TabIndex = 7;
            this.priority.Text = "Priority";
            // 
            // symptom
            // 
            this.symptom.AutoSize = true;
            this.symptom.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.symptom.Location = new System.Drawing.Point(68, 639);
            this.symptom.Name = "symptom";
            this.symptom.Size = new System.Drawing.Size(84, 23);
            this.symptom.TabIndex = 8;
            this.symptom.Text = "Symptom";
            // 
            // title_txt
            // 
            this.title_txt.Location = new System.Drawing.Point(187, 67);
            this.title_txt.Name = "title_txt";
            this.title_txt.Size = new System.Drawing.Size(597, 22);
            this.title_txt.TabIndex = 9;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(187, 109);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(328, 20);
            this.comboBox1.TabIndex = 10;
            // 
            // software_txt
            // 
            this.software_txt.Location = new System.Drawing.Point(187, 145);
            this.software_txt.Name = "software_txt";
            this.software_txt.Size = new System.Drawing.Size(597, 22);
            this.software_txt.TabIndex = 11;
            // 
            // software_version_txt
            // 
            this.software_version_txt.Location = new System.Drawing.Point(187, 183);
            this.software_version_txt.Name = "software_version_txt";
            this.software_version_txt.Size = new System.Drawing.Size(328, 22);
            this.software_version_txt.TabIndex = 12;
            // 
            // description_txt
            // 
            this.description_txt.Location = new System.Drawing.Point(187, 223);
            this.description_txt.Multiline = true;
            this.description_txt.Name = "description_txt";
            this.description_txt.Size = new System.Drawing.Size(597, 146);
            this.description_txt.TabIndex = 13;
            // 
            // step_to_reproduction_txt
            // 
            this.step_to_reproduction_txt.ForeColor = System.Drawing.Color.Black;
            this.step_to_reproduction_txt.Location = new System.Drawing.Point(187, 387);
            this.step_to_reproduction_txt.Multiline = true;
            this.step_to_reproduction_txt.Name = "step_to_reproduction_txt";
            this.step_to_reproduction_txt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.step_to_reproduction_txt.Size = new System.Drawing.Size(597, 165);
            this.step_to_reproduction_txt.TabIndex = 14;
            // 
            // severity_combo
            // 
            this.severity_combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.severity_combo.FormattingEnabled = true;
            this.severity_combo.Location = new System.Drawing.Point(187, 569);
            this.severity_combo.Name = "severity_combo";
            this.severity_combo.Size = new System.Drawing.Size(328, 20);
            this.severity_combo.TabIndex = 15;
            // 
            // priority_combo
            // 
            this.priority_combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.priority_combo.FormattingEnabled = true;
            this.priority_combo.Location = new System.Drawing.Point(187, 604);
            this.priority_combo.Name = "priority_combo";
            this.priority_combo.Size = new System.Drawing.Size(328, 20);
            this.priority_combo.TabIndex = 16;
            // 
            // symptom_txt
            // 
            this.symptom_txt.Location = new System.Drawing.Point(187, 644);
            this.symptom_txt.Multiline = true;
            this.symptom_txt.Name = "symptom_txt";
            this.symptom_txt.Size = new System.Drawing.Size(597, 113);
            this.symptom_txt.TabIndex = 17;
            // 
            // submit_button
            // 
            this.submit_button.Location = new System.Drawing.Point(587, 773);
            this.submit_button.Name = "submit_button";
            this.submit_button.Size = new System.Drawing.Size(75, 23);
            this.submit_button.TabIndex = 18;
            this.submit_button.Text = "Submit";
            this.submit_button.UseVisualStyleBackColor = true;
            this.submit_button.Click += new System.EventHandler(this.submit_button_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(91, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 23);
            this.label1.TabIndex = 19;
            this.label1.Text = "BugID";
            // 
            // bugid_txt
            // 
            this.bugid_txt.ForeColor = System.Drawing.Color.Black;
            this.bugid_txt.Location = new System.Drawing.Point(187, 25);
            this.bugid_txt.Name = "bugid_txt";
            this.bugid_txt.Size = new System.Drawing.Size(597, 22);
            this.bugid_txt.TabIndex = 20;
            // 
            // cancel_button
            // 
            this.cancel_button.Location = new System.Drawing.Point(709, 773);
            this.cancel_button.Name = "cancel_button";
            this.cancel_button.Size = new System.Drawing.Size(75, 23);
            this.cancel_button.TabIndex = 21;
            this.cancel_button.Text = "Cancel";
            this.cancel_button.UseVisualStyleBackColor = true;
            this.cancel_button.Click += new System.EventHandler(this.cancel_button_Click);
            // 
            // AddBugForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 808);
            this.Controls.Add(this.cancel_button);
            this.Controls.Add(this.bugid_txt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.submit_button);
            this.Controls.Add(this.symptom_txt);
            this.Controls.Add(this.priority_combo);
            this.Controls.Add(this.severity_combo);
            this.Controls.Add(this.step_to_reproduction_txt);
            this.Controls.Add(this.description_txt);
            this.Controls.Add(this.software_version_txt);
            this.Controls.Add(this.software_txt);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.title_txt);
            this.Controls.Add(this.symptom);
            this.Controls.Add(this.priority);
            this.Controls.Add(this.severity);
            this.Controls.Add(this.step_to_reproduction);
            this.Controls.Add(this.description);
            this.Controls.Add(this.software_version);
            this.Controls.Add(this.software);
            this.Controls.Add(this.Type);
            this.Controls.Add(this.title);
            this.Name = "AddBugForm";
            this.Text = "AddBugForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label title;
        private System.Windows.Forms.Label Type;
        private System.Windows.Forms.Label software;
        private System.Windows.Forms.Label software_version;
        private System.Windows.Forms.Label description;
        private System.Windows.Forms.Label step_to_reproduction;
        private System.Windows.Forms.Label severity;
        private System.Windows.Forms.Label priority;
        private System.Windows.Forms.Label symptom;
        private System.Windows.Forms.TextBox title_txt;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox software_txt;
        private System.Windows.Forms.TextBox software_version_txt;
        private System.Windows.Forms.TextBox description_txt;
        private System.Windows.Forms.TextBox step_to_reproduction_txt;
        private System.Windows.Forms.ComboBox severity_combo;
        private System.Windows.Forms.ComboBox priority_combo;
        private System.Windows.Forms.TextBox symptom_txt;
        private System.Windows.Forms.Button submit_button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox bugid_txt;
        private System.Windows.Forms.Button cancel_button;
    }
}