﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Assigment2
{
    public partial class Form3 : Form
    {
        public Form3(string username)
        {
            InitializeComponent();
            account_txt.Text = username;
        }

        private void bug_view_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;
            bug_info_panel.Visible = true;
            string constring = "datasource=localhost;username=root;password=";
            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand("select * from bug.bug where Assigned = '"+account_txt.Text+"' ;", conDataBase);

            try
            {


                dataGridView1.Visible = true;
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView1.DataSource = bSource;
                sda.Update(dbdataset);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        } //view bug report accoridng to account

        private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];

                bug_id_txt.Text = row.Cells["Bug_ID"].Value.ToString();
                title_txt.Text = row.Cells["title"].Value.ToString();
                type_of_bug_txt.Text = row.Cells["Type_of_bug"].Value.ToString();
                software_txt.Text = row.Cells["software"].Value.ToString();
                software_version_txt.Text = row.Cells["software_version"].Value.ToString();
                description_txt.Text = row.Cells["step_to_reproduction"].Value.ToString();
                step_to_reproduction_txt.Text = row.Cells["step_to_reproduction"].Value.ToString();
                severity_txt.Text = row.Cells["severity"].Value.ToString();
                priority_txt.Text = row.Cells["priority"].Value.ToString();
                symptom_txt.Text = row.Cells["symptom"].Value.ToString();
                assigned_txt.Text = row.Cells["Assigned"].Value.ToString();
                status_txt.Text = row.Cells["status"].Value.ToString();

            }
        } //display the data from dataGridView

        private void close_bug_Click(object sender, EventArgs e)
        
            {
                closebugForm closebug = new closebugForm();
                closebug.ShowDialog();
            } //Change the bug status to close

        private void logout_button_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Hide();
            Form1 form1 = new Form1();
            form1.ShowDialog();
        } //user logout

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    switch (Convert.ToString(row.Cells[8].Value).Trim())
                    {
                        case "1":
                            row.DefaultCellStyle.BackColor = Color.Yellow;
                            break;
                        case "2":
                            row.DefaultCellStyle.BackColor = Color.LightSteelBlue;
                            break;
                        case "3":
                            row.DefaultCellStyle.BackColor = Color.Crimson;
                            break;
                        case "4":
                            row.DefaultCellStyle.BackColor = Color.DarkGray;
                            break;
                        case "5":
                            row.DefaultCellStyle.BackColor = Color.Crimson;
                            break;
                    }
                }
            }
            catch (SystemException excep)
            {
                MessageBox.Show(excep.Message);
            }
        }//dataGridRow Color

        private void report_bug_Click(object sender, EventArgs e)
        {
            AddBugForm addbug = new AddBugForm();
            addbug.ShowDialog();
        }
    }
}
