﻿namespace Assigment2
{
    partial class closebugForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bug_status_combobox = new System.Windows.Forms.ComboBox();
            this.bug_id_combobox = new System.Windows.Forms.ComboBox();
            this.Submit = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(62, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bug ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(207, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "Bug Status";
            // 
            // bug_status_combobox
            // 
            this.bug_status_combobox.FormattingEnabled = true;
            this.bug_status_combobox.Location = new System.Drawing.Point(174, 54);
            this.bug_status_combobox.Name = "bug_status_combobox";
            this.bug_status_combobox.Size = new System.Drawing.Size(121, 20);
            this.bug_status_combobox.TabIndex = 2;
            // 
            // bug_id_combobox
            // 
            this.bug_id_combobox.FormattingEnabled = true;
            this.bug_id_combobox.Location = new System.Drawing.Point(22, 54);
            this.bug_id_combobox.Name = "bug_id_combobox";
            this.bug_id_combobox.Size = new System.Drawing.Size(121, 20);
            this.bug_id_combobox.TabIndex = 3;
            // 
            // Submit
            // 
            this.Submit.Location = new System.Drawing.Point(346, 30);
            this.Submit.Name = "Submit";
            this.Submit.Size = new System.Drawing.Size(75, 23);
            this.Submit.TabIndex = 4;
            this.Submit.Text = "Submit";
            this.Submit.UseVisualStyleBackColor = true;
            this.Submit.Click += new System.EventHandler(this.Submit_Click);
            // 
            // cancel
            // 
            this.cancel.Location = new System.Drawing.Point(346, 73);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(75, 23);
            this.cancel.TabIndex = 5;
            this.cancel.Text = "Cancel";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // closebugForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 153);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.Submit);
            this.Controls.Add(this.bug_id_combobox);
            this.Controls.Add(this.bug_status_combobox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "closebugForm";
            this.Text = "closebugForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox bug_status_combobox;
        private System.Windows.Forms.ComboBox bug_id_combobox;
        private System.Windows.Forms.Button Submit;
        private System.Windows.Forms.Button cancel;
    }
}