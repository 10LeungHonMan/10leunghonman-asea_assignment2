﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Assigment2
{
    public partial class assignForm : Form
    {
        public assignForm()
        {
            InitializeComponent();
            Fillcombo();
            Fillcombo1();
            Fillcombo2(); 
        }

        void Fillcombo()
        {
            string constring = "datasource = localhost; username = root; password = ";
            string Query = "select Bug_ID from bug.bug";
            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand(Query, conDataBase);
            MySqlDataReader myReader;
            try
            {
                conDataBase.Open();
                myReader = cmdDataBase.ExecuteReader();

                while (myReader.Read())
                {
                    string Bug_ID = myReader.GetString("Bug_ID");
                    Bug_combobox.Items.Add(Bug_ID);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        void Fillcombo1()
        {
            string constring = "datasource = localhost; username = root; password = ";
            string Query = "select ID from logintable.account where permissions = 'developer'";
            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand(Query, conDataBase);
            MySqlDataReader myReader;
            try
            {
                conDataBase.Open();
                myReader = cmdDataBase.ExecuteReader();

                while (myReader.Read())
                {
                  
                    string ID = myReader.GetString("ID");
                    developer_combobox.Items.Add(ID);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        void Fillcombo2()
        {
            string constring = "datasource = localhost; username = root; password = ";
            string Query = "select * from bug.status ";
            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand(Query, conDataBase);
            MySqlDataReader myReader;
            try
            {
                conDataBase.Open();
                myReader = cmdDataBase.ExecuteReader();

                while (myReader.Read())
                {

                    string status = myReader.GetString("bug_status");
                    bug_status_combobox.Items.Add(status);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void assgin_to_button_Click(object sender, EventArgs e)
        {
            string constring = "datasource = localhost; username = root; password = ";
            string Query = "UPDATE bug.bug SET Assigned='" + this.developer_combobox.Text + "', status='"+this.bug_status_combobox.Text+"' where Bug_ID='"+this.Bug_combobox.Text+"'";
            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand(Query, conDataBase);
            MySqlDataReader myReader;
            try
            {
                conDataBase.Open();
                myReader = cmdDataBase.ExecuteReader();
                MessageBox.Show("The Bug information have been updated");
                while (myReader.Read())
                {

                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cancel_button_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
    
}
