﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Assigment2
{
    public partial class closebugForm : Form
    {
        public closebugForm()
        {
            InitializeComponent();
            Fillcombo();
            Fillcombo1();
            
        }

        void Fillcombo()
        {
            string constring = "datasource = localhost; username = root; password = ";
            string Query = "select Bug_ID from bug.bug where status = 'Fixing in Progress' ";
            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand(Query, conDataBase);
            MySqlDataReader myReader;
            try
            {
                conDataBase.Open();
                myReader = cmdDataBase.ExecuteReader();

                while (myReader.Read())
                {
                    string Bug_ID = myReader.GetString("Bug_ID");
                    bug_id_combobox.Items.Add(Bug_ID);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        void Fillcombo1()
        {
            string constring = "datasource = localhost; username = root; password = ";
            string Query = "select * from bug.status where bug_status = 'Fixed' or bug_status = 'Fixing in Progress' ";
            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand(Query, conDataBase);
            MySqlDataReader myReader;
            try
            {
                conDataBase.Open();
                myReader = cmdDataBase.ExecuteReader();

                while (myReader.Read())
                {

                    string status = myReader.GetString("bug_status");
                    bug_status_combobox.Items.Add(status);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void Submit_Click(object sender, EventArgs e)
        {
            string constring = "datasource = localhost; username = root; password = ";
            string Query = "UPDATE bug.bug SET Bug_ID='" + this.bug_id_combobox.Text + "', status='" + this.bug_status_combobox.Text + "' where Bug_ID='" + this.bug_id_combobox.Text + "'";
            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand(Query, conDataBase);
            MySqlDataReader myReader;
            try
            {
                conDataBase.Open();
                myReader = cmdDataBase.ExecuteReader();
                MessageBox.Show("The Bug status has been updated");
                while (myReader.Read())
                {

                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
