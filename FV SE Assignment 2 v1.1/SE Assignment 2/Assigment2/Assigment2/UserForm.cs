﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Assigment2
{
    public partial class UserForm : Form
    {
        public UserForm()
        {
            InitializeComponent();
            dataGridView1.Visible = false;
            bug_info_panel.Visible = false;
        }

        private void report_bug_Click(object sender, EventArgs e) //add bug report
        {
            dataGridView1.Visible = false;
            bug_info_panel.Visible = false;
            AddBugForm addbug = new AddBugForm();
            addbug.ShowDialog();
        }

        private void logout_button_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Hide();
            Form1 form1 = new Form1();
            form1.ShowDialog();
        } //logout

        private void bug_view_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;
            bug_info_panel.Visible = true;

            string constring = "datasource=localhost;username=root;password="; //connect to mysql database
            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand("select * from bug.bug  ;", conDataBase);

            try
            {
                dataGridView1.Visible = true;
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView1.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void bug_status_Click(object sender, EventArgs e)
        {
            ch_bugForm chbug = new ch_bugForm();
            chbug.ShowDialog();
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    switch (Convert.ToString(row.Cells[8].Value).Trim())
                    {
                        case "1":
                            row.DefaultCellStyle.BackColor = Color.Yellow;
                            break;
                        case "2":
                            row.DefaultCellStyle.BackColor = Color.LightSteelBlue;
                            break;
                        case "3":
                            row.DefaultCellStyle.BackColor = Color.Crimson;
                            break;
                        case "4":
                            row.DefaultCellStyle.BackColor = Color.DarkGray;
                            break;
                        case "5":
                            row.DefaultCellStyle.BackColor = Color.Crimson;
                            break;
                    }
                }
            }
            catch (SystemException excep)
            {
                MessageBox.Show(excep.Message);
            }
        }//dataGridRow Color

        private void UserForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Application.Exit();
        }
    }
 }

