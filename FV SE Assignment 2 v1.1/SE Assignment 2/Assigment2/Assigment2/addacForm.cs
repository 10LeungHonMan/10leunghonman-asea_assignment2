﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Assigment2
{
    public partial class addacForm : Form
    {
        public addacForm()
        {
            InitializeComponent();
            Fillcombo();
        }

        void Fillcombo()
        {
            string constring = "datasource = localhost; username = root; password = ";
            string Query = "select * from logintable.permissions";
            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand(Query, conDataBase);
            MySqlDataReader myReader;
            try
            {
                conDataBase.Open();
                myReader = cmdDataBase.ExecuteReader();

                while (myReader.Read())
                {
                    string permissions = myReader.GetString("permissions");
                    permissions_combobox.Items.Add(permissions);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void submit_button_Click(object sender, EventArgs e)
        {
            {
                string constring = "datasource=localhost;username=root;password=";
                string Query = "INSERT INTO logintable.account (ID, Password, permissions) values('" + this.accname_txt.Text + "', '" + this.accpw_txt.Text + "','" + this.permissions_combobox.Text + "')";

                MySqlConnection conDataBase = new MySqlConnection(constring);
                MySqlCommand cmdDataBase = new MySqlCommand(Query, conDataBase);
                MySqlDataReader myReader;
                try
                {
                    conDataBase.Open();
                    myReader = cmdDataBase.ExecuteReader();
                    MessageBox.Show("Saved");
                    while (myReader.Read())
                    {

                    }
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void reset_button_Click(object sender, EventArgs e)
        {
            accname_txt.Clear();
            accpw_txt.Clear();
            permissions_combobox.SelectedIndex = -1;
        }

        private void cancel_button_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
