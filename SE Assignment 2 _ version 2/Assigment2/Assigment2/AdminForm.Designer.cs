﻿namespace Assigment2
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bug_report = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.logout_button = new System.Windows.Forms.Button();
            this.account_info = new System.Windows.Forms.Button();
            this.update_account = new System.Windows.Forms.Button();
            this.delete_account = new System.Windows.Forms.Button();
            this.add_account = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.unfix_button = new System.Windows.Forms.Button();
            this.bug_refresh_button = new System.Windows.Forms.Button();
            this.refresh_button = new System.Windows.Forms.Button();
            this.assign_button = new System.Windows.Forms.Button();
            this.bug_panel = new System.Windows.Forms.Panel();
            this.account_panel = new System.Windows.Forms.Panel();
            this.bug_info_panel = new System.Windows.Forms.Panel();
            this.status_txt = new System.Windows.Forms.TextBox();
            this.assigned_txt = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.symptom_txt = new System.Windows.Forms.TextBox();
            this.priority_txt = new System.Windows.Forms.TextBox();
            this.severity_txt = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.step_to_reproduction_txt = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.description_txt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.software_version_txt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.software_txt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.type_of_bug_txt = new System.Windows.Forms.TextBox();
            this.title_txt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.bug_id_txt = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.bug_panel.SuspendLayout();
            this.account_panel.SuspendLayout();
            this.bug_info_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // bug_report
            // 
            this.bug_report.Location = new System.Drawing.Point(24, 33);
            this.bug_report.Name = "bug_report";
            this.bug_report.Size = new System.Drawing.Size(122, 60);
            this.bug_report.TabIndex = 0;
            this.bug_report.Text = "Bug Report";
            this.bug_report.UseVisualStyleBackColor = true;
            this.bug_report.Click += new System.EventHandler(this.bug_report_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(192, 114);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(686, 117);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.Visible = false;
            this.dataGridView1.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseDoubleClick);
            this.dataGridView1.Click += new System.EventHandler(this.dataGridView1_Click);
            // 
            // logout_button
            // 
            this.logout_button.Location = new System.Drawing.Point(24, 311);
            this.logout_button.Name = "logout_button";
            this.logout_button.Size = new System.Drawing.Size(122, 60);
            this.logout_button.TabIndex = 2;
            this.logout_button.Text = "Logout";
            this.logout_button.UseVisualStyleBackColor = true;
            this.logout_button.Click += new System.EventHandler(this.logout_button_Click);
            // 
            // account_info
            // 
            this.account_info.Location = new System.Drawing.Point(24, 110);
            this.account_info.Name = "account_info";
            this.account_info.Size = new System.Drawing.Size(122, 60);
            this.account_info.TabIndex = 3;
            this.account_info.Text = "Account Management";
            this.account_info.UseVisualStyleBackColor = true;
            this.account_info.Click += new System.EventHandler(this.account_info_Click);
            // 
            // update_account
            // 
            this.update_account.Location = new System.Drawing.Point(172, 22);
            this.update_account.Name = "update_account";
            this.update_account.Size = new System.Drawing.Size(124, 64);
            this.update_account.TabIndex = 5;
            this.update_account.Text = "Edit Account";
            this.update_account.UseVisualStyleBackColor = true;
            this.update_account.Visible = false;
            this.update_account.Click += new System.EventHandler(this.update_account_Click);
            // 
            // delete_account
            // 
            this.delete_account.Location = new System.Drawing.Point(348, 20);
            this.delete_account.Name = "delete_account";
            this.delete_account.Size = new System.Drawing.Size(124, 64);
            this.delete_account.TabIndex = 6;
            this.delete_account.Text = "Delete Account";
            this.delete_account.UseVisualStyleBackColor = true;
            this.delete_account.Visible = false;
            this.delete_account.Click += new System.EventHandler(this.delete_account_Click);
            // 
            // add_account
            // 
            this.add_account.Location = new System.Drawing.Point(3, 22);
            this.add_account.Name = "add_account";
            this.add_account.Size = new System.Drawing.Size(122, 64);
            this.add_account.TabIndex = 4;
            this.add_account.Text = "Add Account";
            this.add_account.UseVisualStyleBackColor = true;
            this.add_account.Visible = false;
            this.add_account.Click += new System.EventHandler(this.add_account_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(199, 114);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowTemplate.Height = 24;
            this.dataGridView2.Size = new System.Drawing.Size(364, 267);
            this.dataGridView2.TabIndex = 7;
            // 
            // unfix_button
            // 
            this.unfix_button.Location = new System.Drawing.Point(3, 23);
            this.unfix_button.Name = "unfix_button";
            this.unfix_button.Size = new System.Drawing.Size(119, 60);
            this.unfix_button.TabIndex = 8;
            this.unfix_button.Text = "New Bug";
            this.unfix_button.UseVisualStyleBackColor = true;
            this.unfix_button.Visible = false;
            this.unfix_button.Click += new System.EventHandler(this.unfix_button_Click);
            // 
            // bug_refresh_button
            // 
            this.bug_refresh_button.Location = new System.Drawing.Point(348, 22);
            this.bug_refresh_button.Name = "bug_refresh_button";
            this.bug_refresh_button.Size = new System.Drawing.Size(124, 60);
            this.bug_refresh_button.TabIndex = 9;
            this.bug_refresh_button.Text = "Refresh";
            this.bug_refresh_button.UseVisualStyleBackColor = true;
            this.bug_refresh_button.Visible = false;
            this.bug_refresh_button.Click += new System.EventHandler(this.bug_refresh_button_Click);
            // 
            // refresh_button
            // 
            this.refresh_button.Location = new System.Drawing.Point(538, 20);
            this.refresh_button.Name = "refresh_button";
            this.refresh_button.Size = new System.Drawing.Size(122, 63);
            this.refresh_button.TabIndex = 10;
            this.refresh_button.Text = "Refresh";
            this.refresh_button.UseVisualStyleBackColor = true;
            this.refresh_button.Visible = false;
            this.refresh_button.Click += new System.EventHandler(this.refresh_button_Click);
            // 
            // assign_button
            // 
            this.assign_button.Location = new System.Drawing.Point(180, 22);
            this.assign_button.Name = "assign_button";
            this.assign_button.Size = new System.Drawing.Size(124, 60);
            this.assign_button.TabIndex = 11;
            this.assign_button.Text = "Update Bug";
            this.assign_button.UseVisualStyleBackColor = true;
            this.assign_button.Visible = false;
            this.assign_button.Click += new System.EventHandler(this.assign_button_Click);
            // 
            // bug_panel
            // 
            this.bug_panel.Controls.Add(this.unfix_button);
            this.bug_panel.Controls.Add(this.assign_button);
            this.bug_panel.Controls.Add(this.bug_refresh_button);
            this.bug_panel.Location = new System.Drawing.Point(187, 12);
            this.bug_panel.Name = "bug_panel";
            this.bug_panel.Size = new System.Drawing.Size(493, 85);
            this.bug_panel.TabIndex = 12;
            this.bug_panel.Visible = false;
            // 
            // account_panel
            // 
            this.account_panel.Controls.Add(this.add_account);
            this.account_panel.Controls.Add(this.update_account);
            this.account_panel.Controls.Add(this.refresh_button);
            this.account_panel.Controls.Add(this.delete_account);
            this.account_panel.Location = new System.Drawing.Point(196, 5);
            this.account_panel.Name = "account_panel";
            this.account_panel.Size = new System.Drawing.Size(796, 88);
            this.account_panel.TabIndex = 13;
            this.account_panel.Visible = false;
            // 
            // bug_info_panel
            // 
            this.bug_info_panel.Controls.Add(this.status_txt);
            this.bug_info_panel.Controls.Add(this.assigned_txt);
            this.bug_info_panel.Controls.Add(this.label12);
            this.bug_info_panel.Controls.Add(this.label11);
            this.bug_info_panel.Controls.Add(this.symptom_txt);
            this.bug_info_panel.Controls.Add(this.priority_txt);
            this.bug_info_panel.Controls.Add(this.severity_txt);
            this.bug_info_panel.Controls.Add(this.label10);
            this.bug_info_panel.Controls.Add(this.label9);
            this.bug_info_panel.Controls.Add(this.label8);
            this.bug_info_panel.Controls.Add(this.step_to_reproduction_txt);
            this.bug_info_panel.Controls.Add(this.label7);
            this.bug_info_panel.Controls.Add(this.description_txt);
            this.bug_info_panel.Controls.Add(this.label6);
            this.bug_info_panel.Controls.Add(this.software_version_txt);
            this.bug_info_panel.Controls.Add(this.label5);
            this.bug_info_panel.Controls.Add(this.label4);
            this.bug_info_panel.Controls.Add(this.software_txt);
            this.bug_info_panel.Controls.Add(this.label3);
            this.bug_info_panel.Controls.Add(this.type_of_bug_txt);
            this.bug_info_panel.Controls.Add(this.title_txt);
            this.bug_info_panel.Controls.Add(this.label2);
            this.bug_info_panel.Controls.Add(this.label1);
            this.bug_info_panel.Controls.Add(this.bug_id_txt);
            this.bug_info_panel.Location = new System.Drawing.Point(191, 234);
            this.bug_info_panel.Name = "bug_info_panel";
            this.bug_info_panel.Size = new System.Drawing.Size(688, 369);
            this.bug_info_panel.TabIndex = 14;
            this.bug_info_panel.Visible = false;
            // 
            // status_txt
            // 
            this.status_txt.Location = new System.Drawing.Point(378, 265);
            this.status_txt.Name = "status_txt";
            this.status_txt.ReadOnly = true;
            this.status_txt.Size = new System.Drawing.Size(100, 22);
            this.status_txt.TabIndex = 23;
            // 
            // assigned_txt
            // 
            this.assigned_txt.Location = new System.Drawing.Point(378, 233);
            this.assigned_txt.Name = "assigned_txt";
            this.assigned_txt.ReadOnly = true;
            this.assigned_txt.Size = new System.Drawing.Size(100, 22);
            this.assigned_txt.TabIndex = 22;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(306, 268);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 12);
            this.label12.TabIndex = 21;
            this.label12.Text = "Status";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(295, 236);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 12);
            this.label11.TabIndex = 20;
            this.label11.Text = "Assigned To";
            // 
            // symptom_txt
            // 
            this.symptom_txt.Location = new System.Drawing.Point(378, 147);
            this.symptom_txt.Multiline = true;
            this.symptom_txt.Name = "symptom_txt";
            this.symptom_txt.ReadOnly = true;
            this.symptom_txt.Size = new System.Drawing.Size(299, 78);
            this.symptom_txt.TabIndex = 19;
            // 
            // priority_txt
            // 
            this.priority_txt.Location = new System.Drawing.Point(378, 119);
            this.priority_txt.Name = "priority_txt";
            this.priority_txt.ReadOnly = true;
            this.priority_txt.Size = new System.Drawing.Size(100, 22);
            this.priority_txt.TabIndex = 18;
            // 
            // severity_txt
            // 
            this.severity_txt.Location = new System.Drawing.Point(378, 88);
            this.severity_txt.Name = "severity_txt";
            this.severity_txt.ReadOnly = true;
            this.severity_txt.Size = new System.Drawing.Size(100, 22);
            this.severity_txt.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(295, 147);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 12);
            this.label10.TabIndex = 16;
            this.label10.Text = "Symptom";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(298, 122);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 12);
            this.label9.TabIndex = 15;
            this.label9.Text = "Priority";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(298, 94);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 12);
            this.label8.TabIndex = 14;
            this.label8.Text = "Severity";
            // 
            // step_to_reproduction_txt
            // 
            this.step_to_reproduction_txt.Location = new System.Drawing.Point(378, 3);
            this.step_to_reproduction_txt.Multiline = true;
            this.step_to_reproduction_txt.Name = "step_to_reproduction_txt";
            this.step_to_reproduction_txt.ReadOnly = true;
            this.step_to_reproduction_txt.Size = new System.Drawing.Size(299, 78);
            this.step_to_reproduction_txt.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(268, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 12);
            this.label7.TabIndex = 12;
            this.label7.Text = "Step to Reproduciton";
            // 
            // description_txt
            // 
            this.description_txt.Location = new System.Drawing.Point(67, 150);
            this.description_txt.Multiline = true;
            this.description_txt.Name = "description_txt";
            this.description_txt.ReadOnly = true;
            this.description_txt.Size = new System.Drawing.Size(184, 156);
            this.description_txt.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 150);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "Description";
            // 
            // software_version_txt
            // 
            this.software_version_txt.Location = new System.Drawing.Point(94, 122);
            this.software_version_txt.Name = "software_version_txt";
            this.software_version_txt.ReadOnly = true;
            this.software_version_txt.Size = new System.Drawing.Size(157, 22);
            this.software_version_txt.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 125);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 12);
            this.label5.TabIndex = 8;
            this.label5.Text = "Software Version";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "Software";
            // 
            // software_txt
            // 
            this.software_txt.Location = new System.Drawing.Point(72, 94);
            this.software_txt.Name = "software_txt";
            this.software_txt.ReadOnly = true;
            this.software_txt.Size = new System.Drawing.Size(179, 22);
            this.software_txt.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "Type of bug";
            // 
            // type_of_bug_txt
            // 
            this.type_of_bug_txt.Location = new System.Drawing.Point(72, 66);
            this.type_of_bug_txt.Name = "type_of_bug_txt";
            this.type_of_bug_txt.ReadOnly = true;
            this.type_of_bug_txt.Size = new System.Drawing.Size(179, 22);
            this.type_of_bug_txt.TabIndex = 4;
            // 
            // title_txt
            // 
            this.title_txt.Location = new System.Drawing.Point(72, 38);
            this.title_txt.Name = "title_txt";
            this.title_txt.ReadOnly = true;
            this.title_txt.Size = new System.Drawing.Size(179, 22);
            this.title_txt.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "Title";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "Bug ID";
            // 
            // bug_id_txt
            // 
            this.bug_id_txt.Location = new System.Drawing.Point(63, 8);
            this.bug_id_txt.Name = "bug_id_txt";
            this.bug_id_txt.ReadOnly = true;
            this.bug_id_txt.Size = new System.Drawing.Size(100, 22);
            this.bug_id_txt.TabIndex = 0;
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(892, 606);
            this.Controls.Add(this.bug_info_panel);
            this.Controls.Add(this.account_panel);
            this.Controls.Add(this.bug_panel);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.account_info);
            this.Controls.Add(this.logout_button);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.bug_report);
            this.DoubleBuffered = true;
            this.Name = "AdminForm";
            this.Text = "AdminForm";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.bug_panel.ResumeLayout(false);
            this.account_panel.ResumeLayout(false);
            this.bug_info_panel.ResumeLayout(false);
            this.bug_info_panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bug_report;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button logout_button;
        private System.Windows.Forms.Button account_info;
        private System.Windows.Forms.Button update_account;
        private System.Windows.Forms.Button delete_account;
        private System.Windows.Forms.Button add_account;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button unfix_button;
        private System.Windows.Forms.Button bug_refresh_button;
        private System.Windows.Forms.Button refresh_button;
        private System.Windows.Forms.Button assign_button;
        private System.Windows.Forms.Panel bug_panel;
        private System.Windows.Forms.Panel account_panel;
        private System.Windows.Forms.Panel bug_info_panel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox step_to_reproduction_txt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox description_txt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox software_version_txt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox software_txt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox type_of_bug_txt;
        private System.Windows.Forms.TextBox title_txt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox bug_id_txt;
        private System.Windows.Forms.TextBox status_txt;
        private System.Windows.Forms.TextBox assigned_txt;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox symptom_txt;
        private System.Windows.Forms.TextBox priority_txt;
        private System.Windows.Forms.TextBox severity_txt;
        private System.Windows.Forms.Label label10;
    }
}