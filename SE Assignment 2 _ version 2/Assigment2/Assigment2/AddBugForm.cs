﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Assigment2
{
   

    public partial class AddBugForm : Form
    {
        public AddBugForm()
        {
            InitializeComponent();
           Fillcombo();
           Fillcombo1();
           Fillcombo2();
        }

        void Fillcombo()
        {
            string constring = "datasource = localhost; username = root; password = ";
            string Query = "select * from bug.type";
            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand(Query, conDataBase);
           MySqlDataReader myReader;
            try
            {
                conDataBase.Open();
                myReader = cmdDataBase.ExecuteReader();

                while (myReader.Read())
                {
                    string type = myReader.GetString("Type_of_bug");
                    comboBox1.Items.Add(type);
                  
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        
         }

        void Fillcombo1()
           {
               string constring1 = "datasource = localhost; username = root; password = ";
               string Query1 = "select * from bug.severity";
               MySqlConnection conDataBase1 = new MySqlConnection(constring1);
               MySqlCommand cmdDataBase1 = new MySqlCommand(Query1, conDataBase1);
               MySqlDataReader myReader;
               try
               {
                   conDataBase1.Open();
                   myReader = cmdDataBase1.ExecuteReader();

                   while (myReader.Read())
                   {

                       string severity = myReader.GetString("severity");
                       severity_combo.Items.Add(severity);

                   }
               }
               catch (Exception ex)
               {
                   MessageBox.Show(ex.Message);
               }

           }
           
        void Fillcombo2()
        {
            string constring2 = "datasource = localhost; username = root; password = ";
            string Query2 = "select * from bug.priority";
            MySqlConnection conDataBase2 = new MySqlConnection(constring2);
            MySqlCommand cmdDataBase2 = new MySqlCommand(Query2, conDataBase2);
            MySqlDataReader myReader;
            try
            {
                conDataBase2.Open();
                myReader = cmdDataBase2.ExecuteReader();

                while (myReader.Read())
                {

                    string priority = myReader.GetString("priority");
                    priority_combo.Items.Add(priority);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

       


        private void submit_button_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(bugid_txt.Text) || !string.IsNullOrEmpty(title_txt.Text) || !string.IsNullOrEmpty(comboBox1.Text) || !string.IsNullOrEmpty(software_txt.Text) || !string.IsNullOrEmpty(software_version_txt.Text) || !string.IsNullOrEmpty(description_txt.Text) || !string.IsNullOrEmpty(step_to_reproduction_txt.Text) || !string.IsNullOrEmpty(severity_combo.Text) || !string.IsNullOrEmpty(priority_combo.Text) || !string.IsNullOrEmpty(symptom_txt.Text)) 
            {

                MessageBox.Show("Please fill in the form!");
                return;
            }

            string constring = "datasource=localhost;username=root;password=";
            string Query = "INSERT INTO bug.bug (Bug_ID, title, Type_of_bug, software, software_version, description, step_to_reproduction, severity, priority, symptom) values('" + this.bugid_txt.Text+"', '" + this.title_txt.Text + "','" + this.comboBox1.Text + "','" + this.software_txt.Text + "','" + this.software_version_txt.Text + "','" + this.description_txt.Text + "','" + this.step_to_reproduction_txt.Text + "','" + this.severity_combo.Text + "','" + this.priority_combo.Text + "','" + this.symptom_txt.Text + "')";
            
            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand(Query, conDataBase);
            MySqlDataReader myReader;
            try
            {
                conDataBase.Open();
                myReader = cmdDataBase.ExecuteReader();
                MessageBox.Show("The Bug have been reported");
                while(myReader.Read())
                {

                }
                this.Close();
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cancel_button_Click(object sender, EventArgs e)
        {
            this.Close();

        }

       
    }
}
