﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace Assigment2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            password_txt.PasswordChar = '‧';

        }

        private void login_button_Click(object sender, EventArgs e)
        {

            try
            {
                bool IsAdminUser = false;
                bool IsDeveloper = false;
                string myConnection = "datasource=localhost;username=root;password=";
                MySqlConnection myConn = new MySqlConnection(myConnection);
                MySqlCommand SelectCommand = new MySqlCommand("select * from logintable.account where id='" + this.username_txt.Text + "' and password='" + this.password_txt.Text + "' ;", myConn);

                MySqlDataReader myReader;

                myConn.Open();
                myReader = SelectCommand.ExecuteReader();
                int count = 0;
                while (myReader.Read())
                {
                    count = count + 1;
                    IsAdminUser = myReader["permissions"].ToString().Equals("Admin");
                    IsDeveloper = myReader["permissions"].ToString().Equals("Developer");
                  
                   
                }

                if (count == 1 && IsDeveloper == true)
                {
                    
                   
                    MessageBox.Show("You are logged in as Developer ");
                    this.Hide();

                    DeveloperForm developform = new DeveloperForm();
                    developform.ShowDialog();
                    
                }

                else if (count == 1 && IsAdminUser == true)
                {
                    MessageBox.Show("You are logged in as administrator ");
                    this.Hide();
                    AdminForm adminForm = new AdminForm();
                    adminForm.ShowDialog();
                }

                else if (count == 1)
                {
                    MessageBox.Show("You are logged in");
                    this.Hide();
                    UserForm userform = new UserForm();
                    userform.ShowDialog();
                }

                else
                    MessageBox.Show("Username or Password is not correct ..Please try again");
                myConn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
