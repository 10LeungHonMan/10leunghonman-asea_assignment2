﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Assigment2
{
    public partial class AdminForm : Form
    {
        public AdminForm()
        {
            InitializeComponent();
            dataGridView1.Visible = true;
            dataGridView2.Visible = false;
            add_account.Visible = false;
            update_account.Visible = false;
            delete_account.Visible = false;
            unfix_button.Visible = false;
            bug_refresh_button.Visible = false;
            assign_button.Visible = false;
            bug_info_panel.Visible = false;
            account_panel.Visible = false;
        }

        void fill_datagrid()
        {
            string myConn1 = "datasource=localhost;username=root;password=";
            MySqlConnection conDataBase2 = new MySqlConnection(myConn1);
            MySqlCommand cmdDataBase1 = new MySqlCommand("select * from logintable.account ;", conDataBase2);
            try
            {
                
                MySqlDataAdapter sda2 = new MySqlDataAdapter();
                sda2.SelectCommand = cmdDataBase1;
                DataTable dbdataset1 = new DataTable();
                sda2.Fill(dbdataset1);
                BindingSource bSource1 = new BindingSource();

                bSource1.DataSource = dbdataset1;
                dataGridView2.DataSource = bSource1;
                sda2.Update(dbdataset1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void fill_datagrid1()
        {
            string myConn2 = "datasource=localhost;username=root;password=";
            MySqlConnection conDataBase3 = new MySqlConnection(myConn2);
            MySqlCommand cmdDataBase2 = new MySqlCommand("select * from bug.bug ;", conDataBase3);
            try
            {

                MySqlDataAdapter sda3 = new MySqlDataAdapter();
                sda3.SelectCommand = cmdDataBase2;
                DataTable dbdataset1 = new DataTable();
                sda3.Fill(dbdataset1);
                BindingSource bSource1 = new BindingSource();

                bSource1.DataSource = dbdataset1;
                dataGridView1.DataSource = bSource1;
                sda3.Update(dbdataset1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void bug_report_Click(object sender, EventArgs e)
        {
            unfix_button.Visible = true;
            bug_refresh_button.Visible = true;
            dataGridView2.Visible = false;
            add_account.Visible = false;
            update_account.Visible = false;
            delete_account.Visible = false;
            refresh_button.Visible = false;
            assign_button.Visible = true;
            bug_panel.Visible = true;
            account_panel.Visible = false;
            bug_info_panel.Visible = false;
            
        }

        private void logout_button_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Hide();
            Form1 form1 = new Form1();
            form1.ShowDialog();
            
            
            //Application.Restart();
            
        }

        private void account_info_Click(object sender, EventArgs e)
        {
            bug_info_panel.Visible = false;
            bug_panel.Visible = false;
            account_panel.Visible = true;
            assign_button.Visible = false;
            dataGridView1.Visible = false;
            dataGridView2.Visible = true;
            add_account.Visible = true;
            update_account.Visible = true;
            delete_account.Visible = true;
            unfix_button.Visible = false;
            bug_refresh_button.Visible = false;
            refresh_button.Visible = true;

            string myConn = "datasource=localhost;username=root;password=";
            MySqlConnection conDataBase1 = new MySqlConnection(myConn);
            MySqlCommand cmdDataBase1 = new MySqlCommand("select * from logintable.account ;", conDataBase1);
            try
            {
                dataGridView2.Visible = true;
                MySqlDataAdapter sda1 = new MySqlDataAdapter();
                sda1.SelectCommand = cmdDataBase1;
                DataTable dbdataset1 = new DataTable();
                sda1.Fill(dbdataset1);
                BindingSource bSource1 = new BindingSource();

                bSource1.DataSource = dbdataset1;
                dataGridView2.DataSource = bSource1;
                sda1.Update(dbdataset1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void unfix_button_Click(object sender, EventArgs e)
        {
            bug_info_panel.Visible = true;
            dataGridView2.Visible = false;
            add_account.Visible = false;
            update_account.Visible = false;
            delete_account.Visible = false;
            refresh_button.Visible = false;
            string constring = "datasource=localhost;username=root;password=";
            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand("select * from bug.bug  ;", conDataBase);

            try
            {
                dataGridView1.Visible = true;
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView1.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void add_account_Click(object sender, EventArgs e)
        {
            addacForm addac = new addacForm();
            addac.ShowDialog();
        }

        private void update_account_Click(object sender, EventArgs e)
        {
            updateacForm updateac = new updateacForm();
            updateac.ShowDialog();
        }

        private void delete_account_Click(object sender, EventArgs e)
        {
            deleteaccForm deleteac = new deleteaccForm();
            deleteac.ShowDialog();
        }

        private void refresh_button_Click(object sender, EventArgs e)
        {

            fill_datagrid();
        }

        private void assign_button_Click(object sender, EventArgs e)
        {
            assignForm assign = new assignForm();
            assign.ShowDialog();
        }

        private void bug_refresh_button_Click(object sender, EventArgs e)
        {
            fill_datagrid1();

        }

       

        private void dataGridView1_Click(object sender, EventArgs e)
        {
           
        }

        private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];

                bug_id_txt.Text = row.Cells["Bug_ID"].Value.ToString();
                title_txt.Text = row.Cells["title"].Value.ToString();
                type_of_bug_txt.Text = row.Cells["Type_of_bug"].Value.ToString();
                software_txt.Text = row.Cells["software"].Value.ToString();
                software_version_txt.Text = row.Cells["software_version"].Value.ToString();
                description_txt.Text = row.Cells["step_to_reproduction"].Value.ToString();
                step_to_reproduction_txt.Text = row.Cells["step_to_reproduction"].Value.ToString();
                severity_txt.Text = row.Cells["severity"].Value.ToString();
                priority_txt.Text = row.Cells["priority"].Value.ToString();
                symptom_txt.Text = row.Cells["symptom"].Value.ToString();
                assigned_txt.Text = row.Cells["Assigned"].Value.ToString();
                status_txt.Text = row.Cells["status"].Value.ToString();

            }
        }
    }
    
}
