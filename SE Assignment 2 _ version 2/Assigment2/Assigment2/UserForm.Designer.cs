﻿namespace Assigment2
{
    partial class UserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.report_bug = new System.Windows.Forms.Button();
            this.logout_button = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // report_bug
            // 
            this.report_bug.Location = new System.Drawing.Point(415, 26);
            this.report_bug.Name = "report_bug";
            this.report_bug.Size = new System.Drawing.Size(184, 41);
            this.report_bug.TabIndex = 0;
            this.report_bug.Text = "Report Bug";
            this.report_bug.UseVisualStyleBackColor = true;
            this.report_bug.Click += new System.EventHandler(this.report_bug_Click);
            // 
            // logout_button
            // 
            this.logout_button.Location = new System.Drawing.Point(632, 26);
            this.logout_button.Name = "logout_button";
            this.logout_button.Size = new System.Drawing.Size(184, 41);
            this.logout_button.TabIndex = 1;
            this.logout_button.Text = "Logout";
            this.logout_button.UseVisualStyleBackColor = true;
            this.logout_button.Click += new System.EventHandler(this.logout_button_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(188, 26);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(184, 41);
            this.button1.TabIndex = 2;
            this.button1.Text = "Bug View";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // UserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(851, 320);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.logout_button);
            this.Controls.Add(this.report_bug);
            this.Name = "UserForm";
            this.Text = "UserForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button report_bug;
        private System.Windows.Forms.Button logout_button;
        private System.Windows.Forms.Button button1;
    }
}