﻿namespace Assigment2
{
    partial class updateacForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.accname = new System.Windows.Forms.Label();
            this.accname_txt = new System.Windows.Forms.TextBox();
            this.acctype = new System.Windows.Forms.Label();
            this.permissions_combobox = new System.Windows.Forms.ComboBox();
            this.submit_button = new System.Windows.Forms.Button();
            this.cancel_button = new System.Windows.Forms.Button();
            this.newacpw = new System.Windows.Forms.Label();
            this.newaccpw_txt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.accpw_txt = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // accname
            // 
            this.accname.AutoSize = true;
            this.accname.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accname.Location = new System.Drawing.Point(48, 49);
            this.accname.Name = "accname";
            this.accname.Size = new System.Drawing.Size(125, 26);
            this.accname.TabIndex = 1;
            this.accname.Text = "User Account";
            // 
            // accname_txt
            // 
            this.accname_txt.Location = new System.Drawing.Point(244, 49);
            this.accname_txt.Multiline = true;
            this.accname_txt.Name = "accname_txt";
            this.accname_txt.Size = new System.Drawing.Size(331, 26);
            this.accname_txt.TabIndex = 3;
            // 
            // acctype
            // 
            this.acctype.AutoSize = true;
            this.acctype.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.acctype.Location = new System.Drawing.Point(48, 260);
            this.acctype.Name = "acctype";
            this.acctype.Size = new System.Drawing.Size(127, 26);
            this.acctype.TabIndex = 6;
            this.acctype.Text = "Account Type";
            // 
            // permissions_combobox
            // 
            this.permissions_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.permissions_combobox.FormattingEnabled = true;
            this.permissions_combobox.Location = new System.Drawing.Point(244, 266);
            this.permissions_combobox.Name = "permissions_combobox";
            this.permissions_combobox.Size = new System.Drawing.Size(199, 20);
            this.permissions_combobox.TabIndex = 9;
            // 
            // submit_button
            // 
            this.submit_button.Location = new System.Drawing.Point(244, 316);
            this.submit_button.Name = "submit_button";
            this.submit_button.Size = new System.Drawing.Size(134, 43);
            this.submit_button.TabIndex = 12;
            this.submit_button.Text = "Submit";
            this.submit_button.UseVisualStyleBackColor = true;
            this.submit_button.Click += new System.EventHandler(this.submit_button_Click);
            // 
            // cancel_button
            // 
            this.cancel_button.Location = new System.Drawing.Point(412, 316);
            this.cancel_button.Name = "cancel_button";
            this.cancel_button.Size = new System.Drawing.Size(134, 43);
            this.cancel_button.TabIndex = 13;
            this.cancel_button.Text = "Cancel";
            this.cancel_button.UseVisualStyleBackColor = true;
            this.cancel_button.Click += new System.EventHandler(this.cancel_button_Click);
            // 
            // newacpw
            // 
            this.newacpw.AutoSize = true;
            this.newacpw.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newacpw.Location = new System.Drawing.Point(12, 184);
            this.newacpw.Name = "newacpw";
            this.newacpw.Size = new System.Drawing.Size(212, 26);
            this.newacpw.TabIndex = 10;
            this.newacpw.Text = "New Account Password";
            // 
            // newaccpw_txt
            // 
            this.newaccpw_txt.Location = new System.Drawing.Point(244, 184);
            this.newaccpw_txt.Multiline = true;
            this.newaccpw_txt.Name = "newaccpw_txt";
            this.newaccpw_txt.Size = new System.Drawing.Size(331, 24);
            this.newaccpw_txt.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(30, 117);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(168, 26);
            this.label1.TabIndex = 14;
            this.label1.Text = "Account Password";
            // 
            // accpw_txt
            // 
            this.accpw_txt.Location = new System.Drawing.Point(244, 117);
            this.accpw_txt.Multiline = true;
            this.accpw_txt.Name = "accpw_txt";
            this.accpw_txt.Size = new System.Drawing.Size(331, 26);
            this.accpw_txt.TabIndex = 15;
            // 
            // updateacForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(619, 393);
            this.Controls.Add(this.accpw_txt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancel_button);
            this.Controls.Add(this.submit_button);
            this.Controls.Add(this.newaccpw_txt);
            this.Controls.Add(this.newacpw);
            this.Controls.Add(this.permissions_combobox);
            this.Controls.Add(this.acctype);
            this.Controls.Add(this.accname_txt);
            this.Controls.Add(this.accname);
            this.Name = "updateacForm";
            this.Text = "updateacForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label accname;
        private System.Windows.Forms.TextBox accname_txt;
        private System.Windows.Forms.Label acctype;
        private System.Windows.Forms.ComboBox permissions_combobox;
        private System.Windows.Forms.Button submit_button;
        private System.Windows.Forms.Button cancel_button;
        private System.Windows.Forms.Label newacpw;
        private System.Windows.Forms.TextBox newaccpw_txt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox accpw_txt;
    }
}