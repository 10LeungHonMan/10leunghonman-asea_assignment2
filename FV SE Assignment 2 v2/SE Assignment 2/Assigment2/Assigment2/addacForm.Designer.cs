﻿namespace Assigment2
{
    partial class addacForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.accname = new System.Windows.Forms.Label();
            this.accpw = new System.Windows.Forms.Label();
            this.accname_txt = new System.Windows.Forms.TextBox();
            this.accpw_txt = new System.Windows.Forms.TextBox();
            this.acctype = new System.Windows.Forms.Label();
            this.reset_button = new System.Windows.Forms.Button();
            this.submit_button = new System.Windows.Forms.Button();
            this.permissions_combobox = new System.Windows.Forms.ComboBox();
            this.cancel_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // accname
            // 
            this.accname.AutoSize = true;
            this.accname.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accname.Location = new System.Drawing.Point(31, 61);
            this.accname.Name = "accname";
            this.accname.Size = new System.Drawing.Size(125, 26);
            this.accname.TabIndex = 0;
            this.accname.Text = "User Account";
            // 
            // accpw
            // 
            this.accpw.AutoSize = true;
            this.accpw.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accpw.Location = new System.Drawing.Point(12, 140);
            this.accpw.Name = "accpw";
            this.accpw.Size = new System.Drawing.Size(168, 26);
            this.accpw.TabIndex = 1;
            this.accpw.Text = "Account Password";
            // 
            // accname_txt
            // 
            this.accname_txt.Location = new System.Drawing.Point(204, 61);
            this.accname_txt.Multiline = true;
            this.accname_txt.Name = "accname_txt";
            this.accname_txt.Size = new System.Drawing.Size(331, 26);
            this.accname_txt.TabIndex = 2;
            // 
            // accpw_txt
            // 
            this.accpw_txt.Location = new System.Drawing.Point(204, 140);
            this.accpw_txt.Multiline = true;
            this.accpw_txt.Name = "accpw_txt";
            this.accpw_txt.Size = new System.Drawing.Size(331, 26);
            this.accpw_txt.TabIndex = 3;
            // 
            // acctype
            // 
            this.acctype.AutoSize = true;
            this.acctype.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.acctype.Location = new System.Drawing.Point(31, 215);
            this.acctype.Name = "acctype";
            this.acctype.Size = new System.Drawing.Size(127, 26);
            this.acctype.TabIndex = 4;
            this.acctype.Text = "Account Type";
            // 
            // reset_button
            // 
            this.reset_button.Location = new System.Drawing.Point(36, 310);
            this.reset_button.Name = "reset_button";
            this.reset_button.Size = new System.Drawing.Size(134, 43);
            this.reset_button.TabIndex = 6;
            this.reset_button.Text = "Reset";
            this.reset_button.UseVisualStyleBackColor = true;
            this.reset_button.Click += new System.EventHandler(this.reset_button_Click);
            // 
            // submit_button
            // 
            this.submit_button.Location = new System.Drawing.Point(204, 310);
            this.submit_button.Name = "submit_button";
            this.submit_button.Size = new System.Drawing.Size(134, 43);
            this.submit_button.TabIndex = 7;
            this.submit_button.Text = "Submit";
            this.submit_button.UseVisualStyleBackColor = true;
            this.submit_button.Click += new System.EventHandler(this.submit_button_Click);
            // 
            // permissions_combobox
            // 
            this.permissions_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.permissions_combobox.FormattingEnabled = true;
            this.permissions_combobox.Location = new System.Drawing.Point(204, 221);
            this.permissions_combobox.Name = "permissions_combobox";
            this.permissions_combobox.Size = new System.Drawing.Size(199, 20);
            this.permissions_combobox.TabIndex = 8;
            // 
            // cancel_button
            // 
            this.cancel_button.Location = new System.Drawing.Point(387, 310);
            this.cancel_button.Name = "cancel_button";
            this.cancel_button.Size = new System.Drawing.Size(134, 43);
            this.cancel_button.TabIndex = 9;
            this.cancel_button.Text = "Cancel";
            this.cancel_button.UseVisualStyleBackColor = true;
            this.cancel_button.Click += new System.EventHandler(this.cancel_button_Click);
            // 
            // addacForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(590, 458);
            this.Controls.Add(this.cancel_button);
            this.Controls.Add(this.permissions_combobox);
            this.Controls.Add(this.submit_button);
            this.Controls.Add(this.reset_button);
            this.Controls.Add(this.acctype);
            this.Controls.Add(this.accpw_txt);
            this.Controls.Add(this.accname_txt);
            this.Controls.Add(this.accpw);
            this.Controls.Add(this.accname);
            this.Name = "addacForm";
            this.Text = "addacForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label accname;
        private System.Windows.Forms.Label accpw;
        private System.Windows.Forms.TextBox accname_txt;
        private System.Windows.Forms.TextBox accpw_txt;
        private System.Windows.Forms.Label acctype;
        private System.Windows.Forms.Button reset_button;
        private System.Windows.Forms.Button submit_button;
        private System.Windows.Forms.ComboBox permissions_combobox;
        private System.Windows.Forms.Button cancel_button;
    }
}