﻿namespace Assigment2
{
    partial class ch_bugForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.bug_id_combobox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.developer_combobox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.bug_status_combobox = new System.Windows.Forms.ComboBox();
            this.change_button = new System.Windows.Forms.Button();
            this.cancel_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(31, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Please Choose a Bug";
            // 
            // bug_id_combobox
            // 
            this.bug_id_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bug_id_combobox.FormattingEnabled = true;
            this.bug_id_combobox.Location = new System.Drawing.Point(26, 88);
            this.bug_id_combobox.Name = "bug_id_combobox";
            this.bug_id_combobox.Size = new System.Drawing.Size(178, 20);
            this.bug_id_combobox.TabIndex = 1;
          
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(31, 146);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "Assign to";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(123, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 23);
            this.label3.TabIndex = 3;
            this.label3.Text = "Developer";
            // 
            // developer_combobox
            // 
            this.developer_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.developer_combobox.FormattingEnabled = true;
            this.developer_combobox.Location = new System.Drawing.Point(26, 204);
            this.developer_combobox.Name = "developer_combobox";
            this.developer_combobox.Size = new System.Drawing.Size(178, 20);
            this.developer_combobox.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(75, 282);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 23);
            this.label4.TabIndex = 5;
            this.label4.Text = "Bug Status";
            // 
            // bug_status_combobox
            // 
            this.bug_status_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bug_status_combobox.FormattingEnabled = true;
            this.bug_status_combobox.Location = new System.Drawing.Point(26, 326);
            this.bug_status_combobox.Name = "bug_status_combobox";
            this.bug_status_combobox.Size = new System.Drawing.Size(178, 20);
            this.bug_status_combobox.TabIndex = 6;
            // 
            // change_button
            // 
            this.change_button.Location = new System.Drawing.Point(12, 391);
            this.change_button.Name = "change_button";
            this.change_button.Size = new System.Drawing.Size(92, 30);
            this.change_button.TabIndex = 7;
            this.change_button.Text = "Change";
            this.change_button.UseVisualStyleBackColor = true;
            this.change_button.Click += new System.EventHandler(this.change_button_Click);
            // 
            // cancel_button
            // 
            this.cancel_button.Location = new System.Drawing.Point(127, 391);
            this.cancel_button.Name = "cancel_button";
            this.cancel_button.Size = new System.Drawing.Size(102, 28);
            this.cancel_button.TabIndex = 8;
            this.cancel_button.Text = "Cancel";
            this.cancel_button.UseVisualStyleBackColor = true;
            this.cancel_button.Click += new System.EventHandler(this.cancel_button_Click);
            // 
            // ch_bugForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 503);
            this.Controls.Add(this.cancel_button);
            this.Controls.Add(this.change_button);
            this.Controls.Add(this.bug_status_combobox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.developer_combobox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bug_id_combobox);
            this.Controls.Add(this.label1);
            this.Name = "ch_bugForm";
            this.Text = "ch_bugForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox bug_id_combobox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox developer_combobox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox bug_status_combobox;
        private System.Windows.Forms.Button change_button;
        private System.Windows.Forms.Button cancel_button;
    }
}