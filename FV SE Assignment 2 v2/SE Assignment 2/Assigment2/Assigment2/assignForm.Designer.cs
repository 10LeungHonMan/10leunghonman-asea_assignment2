﻿namespace Assigment2
{
    partial class assignForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Bug_combobox = new System.Windows.Forms.ComboBox();
            this.bug_select = new System.Windows.Forms.Label();
            this.assign_label = new System.Windows.Forms.Label();
            this.developer_label = new System.Windows.Forms.Label();
            this.developer_combobox = new System.Windows.Forms.ComboBox();
            this.assgin_to_button = new System.Windows.Forms.Button();
            this.bug_status_label = new System.Windows.Forms.Label();
            this.bug_status_combobox = new System.Windows.Forms.ComboBox();
            this.cancel_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Bug_combobox
            // 
            this.Bug_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Bug_combobox.FormattingEnabled = true;
            this.Bug_combobox.Location = new System.Drawing.Point(12, 90);
            this.Bug_combobox.Name = "Bug_combobox";
            this.Bug_combobox.Size = new System.Drawing.Size(158, 20);
            this.Bug_combobox.TabIndex = 1;
            // 
            // bug_select
            // 
            this.bug_select.AutoSize = true;
            this.bug_select.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bug_select.Location = new System.Drawing.Point(39, 52);
            this.bug_select.Name = "bug_select";
            this.bug_select.Size = new System.Drawing.Size(88, 23);
            this.bug_select.TabIndex = 2;
            this.bug_select.Text = "Select Bug";
            // 
            // assign_label
            // 
            this.assign_label.AutoSize = true;
            this.assign_label.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assign_label.Location = new System.Drawing.Point(188, 87);
            this.assign_label.Name = "assign_label";
            this.assign_label.Size = new System.Drawing.Size(81, 23);
            this.assign_label.TabIndex = 3;
            this.assign_label.Text = "Assign To";
            // 
            // developer_label
            // 
            this.developer_label.AutoSize = true;
            this.developer_label.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.developer_label.Location = new System.Drawing.Point(318, 52);
            this.developer_label.Name = "developer_label";
            this.developer_label.Size = new System.Drawing.Size(89, 23);
            this.developer_label.TabIndex = 4;
            this.developer_label.Text = "Developer";
            // 
            // developer_combobox
            // 
            this.developer_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.developer_combobox.FormattingEnabled = true;
            this.developer_combobox.Location = new System.Drawing.Point(290, 90);
            this.developer_combobox.Name = "developer_combobox";
            this.developer_combobox.Size = new System.Drawing.Size(163, 20);
            this.developer_combobox.TabIndex = 5;
            // 
            // assgin_to_button
            // 
            this.assgin_to_button.Location = new System.Drawing.Point(12, 282);
            this.assgin_to_button.Name = "assgin_to_button";
            this.assgin_to_button.Size = new System.Drawing.Size(162, 42);
            this.assgin_to_button.TabIndex = 6;
            this.assgin_to_button.Text = "Press to update";
            this.assgin_to_button.UseVisualStyleBackColor = true;
            this.assgin_to_button.Click += new System.EventHandler(this.assgin_to_button_Click);
            // 
            // bug_status_label
            // 
            this.bug_status_label.AutoSize = true;
            this.bug_status_label.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bug_status_label.Location = new System.Drawing.Point(39, 162);
            this.bug_status_label.Name = "bug_status_label";
            this.bug_status_label.Size = new System.Drawing.Size(91, 23);
            this.bug_status_label.TabIndex = 7;
            this.bug_status_label.Text = "Bug Status";
            // 
            // bug_status_combobox
            // 
            this.bug_status_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bug_status_combobox.FormattingEnabled = true;
            this.bug_status_combobox.Location = new System.Drawing.Point(12, 211);
            this.bug_status_combobox.Name = "bug_status_combobox";
            this.bug_status_combobox.Size = new System.Drawing.Size(158, 20);
            this.bug_status_combobox.TabIndex = 8;
            // 
            // cancel_button
            // 
            this.cancel_button.Location = new System.Drawing.Point(247, 282);
            this.cancel_button.Name = "cancel_button";
            this.cancel_button.Size = new System.Drawing.Size(174, 42);
            this.cancel_button.TabIndex = 9;
            this.cancel_button.Text = "Cancel";
            this.cancel_button.UseVisualStyleBackColor = true;
            this.cancel_button.Click += new System.EventHandler(this.cancel_button_Click);
            // 
            // assignForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 369);
            this.Controls.Add(this.cancel_button);
            this.Controls.Add(this.bug_status_combobox);
            this.Controls.Add(this.bug_status_label);
            this.Controls.Add(this.assgin_to_button);
            this.Controls.Add(this.developer_combobox);
            this.Controls.Add(this.developer_label);
            this.Controls.Add(this.assign_label);
            this.Controls.Add(this.bug_select);
            this.Controls.Add(this.Bug_combobox);
            this.Name = "assignForm";
            this.Text = "assignForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox Bug_combobox;
        private System.Windows.Forms.Label bug_select;
        private System.Windows.Forms.Label assign_label;
        private System.Windows.Forms.Label developer_label;
        private System.Windows.Forms.ComboBox developer_combobox;
        private System.Windows.Forms.Button assgin_to_button;
        private System.Windows.Forms.Label bug_status_label;
        private System.Windows.Forms.ComboBox bug_status_combobox;
        private System.Windows.Forms.Button cancel_button;
    }
}