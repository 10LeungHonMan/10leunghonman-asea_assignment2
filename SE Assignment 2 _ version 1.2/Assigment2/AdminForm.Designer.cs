﻿namespace Assigment2
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bug_report = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.logout_button = new System.Windows.Forms.Button();
            this.account_info = new System.Windows.Forms.Button();
            this.update_account = new System.Windows.Forms.Button();
            this.delete_account = new System.Windows.Forms.Button();
            this.add_account = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.unfix_button = new System.Windows.Forms.Button();
            this.bug_fixed_button = new System.Windows.Forms.Button();
            this.refresh_button = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // bug_report
            // 
            this.bug_report.Location = new System.Drawing.Point(34, 60);
            this.bug_report.Name = "bug_report";
            this.bug_report.Size = new System.Drawing.Size(159, 60);
            this.bug_report.TabIndex = 0;
            this.bug_report.Text = "Bug Report";
            this.bug_report.UseVisualStyleBackColor = true;
            this.bug_report.Click += new System.EventHandler(this.bug_report_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(287, 110);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(832, 472);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.Visible = false;
            // 
            // logout_button
            // 
            this.logout_button.Location = new System.Drawing.Point(34, 540);
            this.logout_button.Name = "logout_button";
            this.logout_button.Size = new System.Drawing.Size(159, 60);
            this.logout_button.TabIndex = 2;
            this.logout_button.Text = "Logout";
            this.logout_button.UseVisualStyleBackColor = true;
            this.logout_button.Click += new System.EventHandler(this.logout_button_Click);
            // 
            // account_info
            // 
            this.account_info.Location = new System.Drawing.Point(34, 153);
            this.account_info.Name = "account_info";
            this.account_info.Size = new System.Drawing.Size(159, 60);
            this.account_info.TabIndex = 3;
            this.account_info.Text = "Account Management";
            this.account_info.UseVisualStyleBackColor = true;
            this.account_info.Click += new System.EventHandler(this.account_info_Click);
            // 
            // update_account
            // 
            this.update_account.Location = new System.Drawing.Point(491, 33);
            this.update_account.Name = "update_account";
            this.update_account.Size = new System.Drawing.Size(165, 71);
            this.update_account.TabIndex = 5;
            this.update_account.Text = "Edit Account";
            this.update_account.UseVisualStyleBackColor = true;
            this.update_account.Visible = false;
            this.update_account.Click += new System.EventHandler(this.update_account_Click);
            // 
            // delete_account
            // 
            this.delete_account.Location = new System.Drawing.Point(703, 33);
            this.delete_account.Name = "delete_account";
            this.delete_account.Size = new System.Drawing.Size(174, 71);
            this.delete_account.TabIndex = 6;
            this.delete_account.Text = "Delete Account";
            this.delete_account.UseVisualStyleBackColor = true;
            this.delete_account.Visible = false;
            this.delete_account.Click += new System.EventHandler(this.delete_account_Click);
            // 
            // add_account
            // 
            this.add_account.Location = new System.Drawing.Point(287, 33);
            this.add_account.Name = "add_account";
            this.add_account.Size = new System.Drawing.Size(164, 71);
            this.add_account.TabIndex = 4;
            this.add_account.Text = "Add Account";
            this.add_account.UseVisualStyleBackColor = true;
            this.add_account.Visible = false;
            this.add_account.Click += new System.EventHandler(this.add_account_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(287, 123);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowTemplate.Height = 24;
            this.dataGridView2.Size = new System.Drawing.Size(350, 316);
            this.dataGridView2.TabIndex = 7;
            // 
            // unfix_button
            // 
            this.unfix_button.Location = new System.Drawing.Point(287, 12);
            this.unfix_button.Name = "unfix_button";
            this.unfix_button.Size = new System.Drawing.Size(164, 71);
            this.unfix_button.TabIndex = 8;
            this.unfix_button.Text = "New Bug";
            this.unfix_button.UseVisualStyleBackColor = true;
            this.unfix_button.Visible = false;
            this.unfix_button.Click += new System.EventHandler(this.unfix_button_Click);
            // 
            // bug_fixed_button
            // 
            this.bug_fixed_button.Location = new System.Drawing.Point(510, 12);
            this.bug_fixed_button.Name = "bug_fixed_button";
            this.bug_fixed_button.Size = new System.Drawing.Size(164, 71);
            this.bug_fixed_button.TabIndex = 9;
            this.bug_fixed_button.Text = "Fixed Bug";
            this.bug_fixed_button.UseVisualStyleBackColor = true;
            this.bug_fixed_button.Visible = false;
            // 
            // refresh_button
            // 
            this.refresh_button.Location = new System.Drawing.Point(930, 33);
            this.refresh_button.Name = "refresh_button";
            this.refresh_button.Size = new System.Drawing.Size(174, 71);
            this.refresh_button.TabIndex = 10;
            this.refresh_button.Text = "Refresh";
            this.refresh_button.UseVisualStyleBackColor = true;
            this.refresh_button.Visible = false;
            this.refresh_button.Click += new System.EventHandler(this.refresh_button_Click);
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1116, 641);
            this.Controls.Add(this.refresh_button);
            this.Controls.Add(this.bug_fixed_button);
            this.Controls.Add(this.unfix_button);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.delete_account);
            this.Controls.Add(this.add_account);
            this.Controls.Add(this.update_account);
            this.Controls.Add(this.account_info);
            this.Controls.Add(this.logout_button);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.bug_report);
            this.Name = "AdminForm";
            this.Text = "AdminForm";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bug_report;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button logout_button;
        private System.Windows.Forms.Button account_info;
        private System.Windows.Forms.Button update_account;
        private System.Windows.Forms.Button delete_account;
        private System.Windows.Forms.Button add_account;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button unfix_button;
        private System.Windows.Forms.Button bug_fixed_button;
        private System.Windows.Forms.Button refresh_button;
    }
}