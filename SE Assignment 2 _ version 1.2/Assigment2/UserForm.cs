﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Assigment2
{
    public partial class UserForm : Form
    {
        public UserForm()
        {
            InitializeComponent();
        }

        private void report_bug_Click(object sender, EventArgs e)
        {
            AddBugForm addbug = new AddBugForm();
            addbug.ShowDialog();
        }

        private void logout_button_Click(object sender, EventArgs e)
        {
           this.Close();
           Application.Restart();

        }
    }
}
