﻿namespace Assigment2
{
    partial class deleteaccForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.accname = new System.Windows.Forms.Label();
            this.accname_txt = new System.Windows.Forms.TextBox();
            this.accpw = new System.Windows.Forms.Label();
            this.accpw_txt = new System.Windows.Forms.TextBox();
            this.delete_button = new System.Windows.Forms.Button();
            this.cancel_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // accname
            // 
            this.accname.AutoSize = true;
            this.accname.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accname.Location = new System.Drawing.Point(43, 53);
            this.accname.Name = "accname";
            this.accname.Size = new System.Drawing.Size(125, 26);
            this.accname.TabIndex = 1;
            this.accname.Text = "User Account";
            // 
            // accname_txt
            // 
            this.accname_txt.Location = new System.Drawing.Point(190, 53);
            this.accname_txt.Multiline = true;
            this.accname_txt.Name = "accname_txt";
            this.accname_txt.Size = new System.Drawing.Size(331, 26);
            this.accname_txt.TabIndex = 3;
            // 
            // accpw
            // 
            this.accpw.AutoSize = true;
            this.accpw.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accpw.Location = new System.Drawing.Point(12, 111);
            this.accpw.Name = "accpw";
            this.accpw.Size = new System.Drawing.Size(168, 26);
            this.accpw.TabIndex = 4;
            this.accpw.Text = "Account Password";
            // 
            // accpw_txt
            // 
            this.accpw_txt.Location = new System.Drawing.Point(190, 111);
            this.accpw_txt.Multiline = true;
            this.accpw_txt.Name = "accpw_txt";
            this.accpw_txt.Size = new System.Drawing.Size(331, 26);
            this.accpw_txt.TabIndex = 5;
            // 
            // delete_button
            // 
            this.delete_button.Location = new System.Drawing.Point(190, 169);
            this.delete_button.Name = "delete_button";
            this.delete_button.Size = new System.Drawing.Size(134, 43);
            this.delete_button.TabIndex = 8;
            this.delete_button.Text = "Delete";
            this.delete_button.UseVisualStyleBackColor = true;
            this.delete_button.Click += new System.EventHandler(this.delete_button_Click);
            // 
            // cancel_button
            // 
            this.cancel_button.Location = new System.Drawing.Point(387, 169);
            this.cancel_button.Name = "cancel_button";
            this.cancel_button.Size = new System.Drawing.Size(134, 43);
            this.cancel_button.TabIndex = 10;
            this.cancel_button.Text = "Cancel";
            this.cancel_button.UseVisualStyleBackColor = true;
            // 
            // deleteaccForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 246);
            this.Controls.Add(this.cancel_button);
            this.Controls.Add(this.delete_button);
            this.Controls.Add(this.accpw_txt);
            this.Controls.Add(this.accpw);
            this.Controls.Add(this.accname_txt);
            this.Controls.Add(this.accname);
            this.Name = "deleteaccForm";
            this.Text = " ";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label accname;
        private System.Windows.Forms.TextBox accname_txt;
        private System.Windows.Forms.Label accpw;
        private System.Windows.Forms.TextBox accpw_txt;
        private System.Windows.Forms.Button delete_button;
        private System.Windows.Forms.Button cancel_button;
    }
}