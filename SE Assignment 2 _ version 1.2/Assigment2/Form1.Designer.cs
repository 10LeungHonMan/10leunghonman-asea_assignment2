﻿namespace Assigment2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.username_txt = new System.Windows.Forms.TextBox();
            this.username = new System.Windows.Forms.Label();
            this.login_button = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.password = new System.Windows.Forms.Label();
            this.password_txt = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // username_txt
            // 
            this.username_txt.Location = new System.Drawing.Point(156, 167);
            this.username_txt.Name = "username_txt";
            this.username_txt.Size = new System.Drawing.Size(235, 22);
            this.username_txt.TabIndex = 1;
            // 
            // username
            // 
            this.username.AutoSize = true;
            this.username.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.username.Location = new System.Drawing.Point(64, 165);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(90, 19);
            this.username.TabIndex = 2;
            this.username.Text = "Username";
            // 
            // login_button
            // 
            this.login_button.Location = new System.Drawing.Point(206, 269);
            this.login_button.Name = "login_button";
            this.login_button.Size = new System.Drawing.Size(75, 23);
            this.login_button.TabIndex = 0;
            this.login_button.Text = "Login";
            this.login_button.UseVisualStyleBackColor = true;
            this.login_button.Click += new System.EventHandler(this.login_button_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 403);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(476, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // password
            // 
            this.password.AutoSize = true;
            this.password.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.password.Location = new System.Drawing.Point(64, 203);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(86, 19);
            this.password.TabIndex = 5;
            this.password.Text = "Password";
            // 
            // password_txt
            // 
            this.password_txt.Location = new System.Drawing.Point(155, 205);
            this.password_txt.Name = "password_txt";
            this.password_txt.Size = new System.Drawing.Size(236, 22);
            this.password_txt.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 425);
            this.Controls.Add(this.password);
            this.Controls.Add(this.password_txt);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.username);
            this.Controls.Add(this.username_txt);
            this.Controls.Add(this.login_button);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox username_txt;
        private System.Windows.Forms.Label username;
        private System.Windows.Forms.Button login_button;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Label password;
        private System.Windows.Forms.TextBox password_txt;
    }
}

